

const burgerMenu = document.querySelector('.hamburger-menu');
const navCollapse = document.querySelector('header');

burgerMenu.addEventListener('click', openNav);

function openNav() {
  burgerMenu.classList.toggle('open');
  navCollapse.classList.toggle('navCollapse');
}
