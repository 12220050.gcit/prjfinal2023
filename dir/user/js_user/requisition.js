const button = document.querySelector("#submitBtn");
//getting the element from the html file
const staffIdE1 = document.querySelector("#staffId");
// const passwordE1 = document.querySelector("#Password");
const form = document.querySelector("#container1");

button.addEventListener("click", function (e) {
  let isstaffIdValid = checkstaffId();
  let isFormValid = isstaffIdValid; //submit to server if the form is valid
  if (isFormValid) {
    var formData = readFormData();
    if (selectedRow == null) insertNewRecord(formData);
    else updateFormData(formData);
    resetForm();
    // var formDa
    confirm("Are you sure you want to submit."); //to get final confirmation or to reset the data if the information is wrong.
    return true;
  } else {
    e.preventDefault(); //prevent the form from submitting
  }
});
//function to validate the staffId
const checkstaffId = () => {
  let valid = false;
  const min = 3,
    max = 25;
  const staffId = staffIdE1.value.trim(); //trim is going to remove the space before and after the text and give text only
  //calling the showerror function with two arguments
  if (!isRequired(staffId)) {
    showError(staffIdE1, "staffId cannot be blank.");
  } else if (!isBetween(staffId.length, min, max)) {
    showError(
      staffIdE1,
      `staffId must be between ${min} and ${max} characters.`
    );
  } else if (!isstaffIdValid(staffId)) {
    showError(staffIdE1, "staffId is not valid! ");
    //calling the showsuccess function with two arguments
  } else {
    showSuccess(staffIdE1);
    valid = true;
  }
  return valid;
};

//Regular expression to validate the student name pattern
const isstaffIdValid = (staffId) => {
  const re =/^rub\d{7}$/;

    // /^([A-Z]{1}[a-zA-Z]+)(\s[A-Z]{1}[a-zA-Z]+)?(\s[A-Z]{1}[a-zA-Z]+)?$/;
  return re.test(staffId);
};

//reusable code
//Function to check whether the input section is empty or not
const isRequired = (value) => (value === "" ? false : true);
//Function to check the length of the the character in the input section
const isBetween = (length, min, max) =>
  length < min || length > max ? false : true;

//reusable code
//Regulating the css classes according to the validation
const showError = (input, message) => {
  //get the form field element
  const formField = input.parentElement;
  //add the error class
  formField.classList.remove("success");
  formField.classList.add("error");
  //show the error message
  const error = formField.querySelector("small");
  error.textContent = message;
};

const showSuccess = (input) => {
  //get the form field element
  const formField = input.parentElement;
  //remove the error class
  formField.classList.remove("error");
  formField.classList.add("success");
  //hide the error message
  const error = formField.querySelector("small");
  error.textContent = "";
};

function readFormData() {
  var formData = {};
  formData["staffId"] = document.getElementById("staffId").value;
  formData["ItemName"] = document.getElementById("ItemName").value;
  formData["Quantity"] = document.getElementById("Quantity").value;
  formData["LoanDate"] = document.getElementById("LoanDate").value;
  formData["reason"] = document.getElementById("reason").value;
  formData["status"] = Status;
  return formData;
}

var SlNo = 0;
var Status = "Pending";
function insertNewRecord(data) {
  SlNo++;
  var table = document
    .getElementById("stdList")
    .getElementsByTagName("tbody")[0];
  var newRow = table.insertRow(table.lenght);
  cell1 = newRow.insertCell(0);
  cell1.innerHTML = SlNo;

  cell2 = newRow.insertCell(1);
  cell2.innerHTML = data.staffId;

  cell3 = newRow.insertCell(2);
  cell3.innerHTML = data.ItemName;

  cell5 = newRow.insertCell(3);
  cell5.innerHTML = data.Quantity;

  cell5 = newRow.insertCell(4);
  cell5.innerHTML = data.reason;

  cell6 = newRow.insertCell(5);
  cell6.innerHTML = Status;

  cell7 = newRow.insertCell(6);
  cell7.innerHTML = '<a onClick="onEdit(this)">Edit</a>';
  cell7 = newRow.insertCell(6);
  cell7.innerHTML = '<a onClick="onDelete(this)">Delete</a>';
}

function onEdit(td) {
  selectedRow = td.parentElement.parentElement;
  document.getElementById("staffId").value = selectedRow.cells[0].innerHTML;
  document.getElementById("ItemName").value = selectedRow.cells[1].innerHTML;
  document.getElementById("Quantity").value = selectedRow.cells[2].innerHTML;
  document.getElementById("LoanDate").value = selectedRow.cells[3].innerHTML;
  document.getElementById("reason").value = selectedRow.cells[4].innerHTML;
  // document.getElementById("status").value = selectedRow.cells[7].innerHTML;
}

var selectedRow = null;
function resetForm() {
  document.getElementById("staffId").value ="";
  document.getElementById("ItemName").value ="";
  document.getElementById("Quantity").value ="";
  document.getElementById("LoanDate").value ="";
  document.getElementById("reason").value ="";
  // document.getElementById("status").value = "";
  selectedRow = null;
}

function updateFormData(formData) {
  selectedRow.cells[0].innerHTML = formData.staffId;
  selectedRow.cells[1].innerHTML = formData.ItemName;
  selectedRow.cells[2].innerHTML = formData.Quantity;
  selectedRow.cells[3].innerHTML = formData.LoanDate;
  selectedRow.cells[4].innerHTML = formData.leaveStart;
  selectedRow.cells[5].innerHTML = formData.reason;
  // selectedRow.cells[7].innerHTML = formData.status;
}

function onDelete(td) {
  if (confirm("Are You Sure to DELETE this record")) {
    row = td.parentElement.parentElement;
    document.getElementById("stdList").deleteRow(row.rowIndex);
    resetForm();
  }
}
